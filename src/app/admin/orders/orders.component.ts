import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {

  constructor(private api:ApiService,private toastr: ToastrService) { }
  orders:any[]=[];
  
  ngOnInit(): void {
    this.getAllOrders()
  }

  getAllOrders()
  {
    let url = "https://localhost:44337/api/order";
    this.api.get(url).subscribe(res=>{
      let response = JSON.parse(JSON.stringify(res));
      let that=this
      response.forEach(function(order:any){
        let productId=order.productId;
        let customerId = order.customerId;
        let productUrl = `https://localhost:44337/api/products/${productId}`;
        let customerUrl = `https://localhost:44337/api/customer/${customerId}`
        that.api.get(productUrl).subscribe(r=>{
          let prodRes = JSON.parse(JSON.stringify(r));
            that.api.get(customerUrl).subscribe(o=>{
              let custRes = JSON.parse(JSON.stringify(o))
             console.log(order)
              let obj ={
                "orderId":order.orderId,
                "productName":prodRes.productName,
                "customer":custRes.customerEmailId,
                "quantity":order.orderQuantity,
                "price":order.orderPrice,
                "address":order.shipmentAddress

              }
              that.orders.push(obj)
              console.log(that.orders)
            })
        })
        
      })

      


    })
  }
  trackByIndex = (index:number):number =>{
    return index;
  }
}
