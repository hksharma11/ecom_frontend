import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OrdersComponent } from './admin/orders/orders.component';
import { CreateCategoryComponent } from './home/create-category/create-category.component';
import { CreateProductComponent } from './home/create-product/create-product.component';
import { ProductComponent } from './home/product/product.component';
import { LoginComponent } from './user/login/login.component';
import { MyOrdersComponent } from './user/my-orders/my-orders.component';
import { PlaceOrderComponent } from './user/place-order/place-order.component';
import { RegisterComponent } from './user/register/register.component';

const routes: Routes = [
  {path:'place-order',component:PlaceOrderComponent},
  {path:'my-orders',component:MyOrdersComponent},
  {path:'orders',component:OrdersComponent},
  {path:'product',component:ProductComponent},
  {path:"register", component:RegisterComponent},
  {path:"login",component:LoginComponent},
  {path:"create-product",component:CreateProductComponent},
  {path:"create-category",component:CreateCategoryComponent},
  {path:"**", component:ProductComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
