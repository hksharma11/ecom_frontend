import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ApiService } from 'src/app/services/api.service';
import { DataService } from 'src/app/services/data.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private dataservice:DataService,private formbuilder:FormBuilder,private toastr: ToastrService,private api:ApiService,private router:Router) { }

  ngOnInit(): void {
  }

  myForm: FormGroup = this.formbuilder.group({
    email: ["", [Validators.required]],
    password:["", [Validators.required]],
  
  })


  send()
  {
    if(this.myForm.status=='VALID')
    {
      this.api.get(`https://localhost:44304/api/Login/GetLoginByUserId/${this.myForm.value.email}`).subscribe(res=>{
        if(res==null)
        {
          this.toastr.info("User Not Registered");
        }else{
          console.log(res);
          let response = JSON.parse(JSON.stringify(res));
          if(response.password == this.myForm.value.password)
          {
            let loginId = response.loginId;
            let customerUrl = `https://localhost:44337/api/customer/login/${loginId}`
            this.api.get(customerUrl).subscribe(r=>{
              let custRes = JSON.parse(JSON.stringify(r));
              console.log("cust",custRes);
              let body = {
                loginId:response.loginId,
                loginRole:response.loginRole,
                token:response.token,
                userId:response.userId,
                customerId:custRes.customerId,
                address:custRes.customerAddress
              }
              localStorage.setItem('User', JSON.stringify(body));
              this.dataservice.setUser(body)
              this.router.navigateByUrl('/product');
              
            })
            
          }else{
            this.toastr.warning("Invalid Credentials")
          }
        }
      })

    }else{
      this.toastr.error("All Field Are Required")
    }

  }

}
