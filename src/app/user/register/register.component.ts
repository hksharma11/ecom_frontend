import { Component, OnInit } from '@angular/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  constructor(private formbuilder:FormBuilder,private toastr: ToastrService,private api:ApiService,private router:Router) { }

  ngOnInit(): void {
  }

  myForm: FormGroup = this.formbuilder.group({
    name: ["", [Validators.required]],
    email: ["", [Validators.required]],
    phone:["", [Validators.required]],
    address:["", [Validators.required]],
    password:["", [Validators.required]],
    confirmpassword: ["", [Validators.required]],
    
  })

  send()
  {
    console.log(this.myForm)
    if(this.myForm.status=='VALID')
    {
        if(this.myForm.value.password == this.myForm.value.confirmpassword)
        {
          this.api.get(`https://localhost:44304/api/Login/GetLoginByUserId/${this.myForm.value.email}`).subscribe(res=>{
            if(res == null)
            {
              let url = `https://localhost:44337/api/login/${this.myForm.value.password}/testtoken/2015-05-16 05:50:06.000/User/${this.myForm.value.email}`;

           this.api.post(url).subscribe(res=>{
            console.log('test',res);
            let response = JSON.parse(JSON.stringify(res));
            let loginId = response.loginId;
            let addcustomerurl =`https://localhost:44337/api/AddCustomer/${this.myForm.value.name}/${this.myForm.value.address}/${this.myForm.value.phone}/${this.myForm.value.email}/${loginId}`;
            this.api.post(addcustomerurl).subscribe(response=>{
              this.toastr.success("User Created");
              this.router.navigateByUrl('/login');
            })
            
           })
            }else{
              this.toastr.error("User Already Exist");
            }
          })
          
          


        }else{
          this.toastr.warning("Both Password should be same!")
        }
    }else{
        this.toastr.error("Invalid Details")
    }
  }

}
